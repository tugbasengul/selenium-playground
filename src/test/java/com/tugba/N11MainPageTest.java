
package com.tugba;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertTrue;

/**
 * Created by tugba on 12.03.2016.
 */
public class N11MainPageTest {

    public static final String HTTP_WWW_N11_COM = "http://www.n11.com/";

    private WebDriver webDriver;

    @Before
    public void setUp() throws Exception {
        webDriver = new FirefoxDriver();

    }


    @After
    public void tearDown() throws Exception {

        webDriver.close();
        webDriver.quit();


    }

    public void goToHomePage() {

        webDriver.get(HTTP_WWW_N11_COM);

    }

    public void goToCampaign() {

        webDriver.get(HTTP_WWW_N11_COM + "kampanyalar");
    }

    @Test
    public void shouldGetMainPageTitle() {

        goToHomePage();
        System.out.println(webDriver.getTitle());

        //webdriver'ı kapat

    }

    @Test
    public void shouldCheckMostSellerProductTitle() {

        goToHomePage();

        WebElement webElement = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/section[1]/h2"));

        System.out.println(webElement.getText());


    }

    @Test
    public void shouldCheckHeaderCampaignUrlIsWrongTest() {

        goToHomePage();

        WebElement headerCampaignBannerElement = webDriver.findElement(By.id("headerCampaignBnr"));

        System.out.println(headerCampaignBannerElement.getAttribute("title"));
        System.out.println(headerCampaignBannerElement.getAttribute("href"));


    }


    @Test
    public void shouldGetCampaignPageTitle() {

        goToCampaign();
        WebElement campaingTitle = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/h1"));

        assertThat(campaingTitle.getText(), equalTo("Kampanyalar ve Promosyonlar"));


    }

    @Test
    public void shouldGetCampaignCount() {

        goToCampaign();

        //Class'ı slidesjs-pagination olan UL html elementini bul
        WebElement ul = webDriver.findElement(By.className("slidesjs-pagination"));

        // Buldugumuz ul elementinin içindeli li elementlerini bul
        // List<WebElement> elements = ul.findElements(By.tagName("li"));

        //Bulduğumuz ul elementinin içindeki classname'i slidesjs-pagination-item olanları bul
        List<WebElement> elements = ul.findElements(By.className("slidesjs-pagination-item"));

        assertThat(elements.size(), equalTo(3));


    }

    @Test
    public void shouldCheckMainPagePromotionBanner() {


        goToHomePage();

        WebElement topBanners = webDriver.findElement(By.className("topBanners"));

        WebElement ul = topBanners.findElement(By.tagName("ul"));

        List<WebElement> liList = ul.findElements(By.tagName("li"));

        assertThat(liList.size(), equalTo(3));

        WebElement firstCampaign = liList.get(0).findElement(By.tagName("a"));
        WebElement secondCampaign = liList.get(1).findElement(By.tagName("a"));
        WebElement thirdhCampaign = liList.get(2).findElement(By.tagName("a"));

        assertThat(firstCampaign.getAttribute("title"), equalTo("Tomurcukbebe'den %20 İNDİRİM Altın ve Gümüş Takılarda Kampanya"));
        assertThat(secondCampaign.getAttribute("title"), equalTo("Tengo ile Eviniz Renklensin"));
        assertThat(thirdhCampaign.getAttribute("title"), equalTo("Avon Ürünlerinde 15 TL İndirim"));


    }

    @Test
    public void shouldCheckMostSearchKeywords() {

        goToHomePage();

        WebElement mostSearchKeywordLabel = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/h6"));
        WebElement ul = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/ul"));
        List<WebElement> li = ul.findElements(By.tagName("li"));

        assertThat(mostSearchKeywordLabel.getText(), equalTo("En Çok Aranan Kelimeler"));
        assertThat(li.size(), equalTo(12));


    }

    @Test
    public void shouldCheckMarket11TabMenu() throws Exception {


        goToHomePage();

        List<WebElement> elements = webDriver.findElements(By.cssSelector("html body div#wrapper.wrapper.home div#contentMain.content div.container section.group.marketGroup div.tab ul li h3 a"));
        List<String>  tabNameList = new ArrayList<String>();
        List<String>  tabUrlList = new ArrayList<String>();

        for (WebElement eachWebElement : elements) {
            tabNameList.add(eachWebElement.getText());
            tabUrlList.add(eachWebElement.getAttribute("href"));
        }




        assertTrue(tabNameList.contains("Çok Satanlar"));
        assertTrue(tabUrlList.contains("http://www.n11.com/market11/arama?q=Supermarketim&rf=market11"));

        assertTrue(tabNameList.contains("Yeni Ürünler"));
        assertTrue(tabUrlList.contains("http://www.n11.com/market11/arama?q=Supermarketim&srt=NEWEST&rf=market11"));

        assertTrue(tabNameList.contains("Manav"));
        assertTrue(tabUrlList.contains("http://www.n11.com/market11?rf=market11"));

        assertTrue(tabNameList.contains("Bebek Bezi"));
        assertTrue(tabUrlList.contains("http://www.n11.com/market11/bebek/bez-islak-mendil/bez?rf=market11"));

        assertTrue(tabNameList.contains("Şampuan"));
        assertTrue(tabUrlList.contains("http://www.n11.com/market11/kozmetik-bakim/sac-bakimi-sekillendiriciler?q=Supermarketim&rf=market11"));

        assertTrue(tabNameList.contains("Çamaşır Deterjanı"));
        assertTrue(tabUrlList.contains("http://www.n11.com/market11/deterjan-temizlik/camasir-yikama?q=Supermarketim&rf=market11"));

        assertTrue(tabNameList.contains("Sıcak İçecek"));
        assertTrue(tabUrlList.contains("http://www.n11.com/market11/gida-icecek/sicak-icecek?q=Supermarketim&rf=market11"));

        assertTrue(tabNameList.contains("Tıraş ve Tıraş Sonrası"));
        assertTrue(tabUrlList.contains("http://www.n11.com/market11/kozmetik-bakim/tiras-ve-tiras-sonrasi?rf=market11"));


    }

    @Test
    public void shouldCheckBestSellerTabs() {


        webDriver.get(HTTP_WWW_N11_COM + "cok-satanlar");


        WebElement headerBestSellerGroup = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div[1]/h1"));

        assertThat(headerBestSellerGroup.getText(), equalTo("Tüm Kategoriler"));

        WebElement ul = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div[3]/ul"));

        List<WebElement> li = ul.findElements(By.tagName("li"));
        List<WebElement> elements = webDriver.findElements(By.cssSelector("html body div#wrapper.wrapper div#contentBestSaller.content div.container div.group.bestSellerGroup div.tab.categoryMenuTab ul li a"));

        assertThat(li.size(), equalTo(11));

        List<String> categoryNameList = new ArrayList<String>();
        List<String> categoryUrlList = new ArrayList<String>();

        //tum elementlerin icinde don, donerken her donusunde eachWebElement degiskenine ata
        for (int i = 0; i < elements.size(); i++) {
            categoryNameList.add(elements.get(i).getText());
            categoryUrlList.add(elements.get(i).getAttribute("href"));
        }

        for (WebElement eachWebElement : elements) {
            categoryNameList.add(eachWebElement.getText());
            categoryUrlList.add(eachWebElement.getAttribute("href"));
        }

        assertTrue(categoryNameList.contains("Tüm Kategoriler"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar"));

        assertTrue(categoryNameList.contains("Giyim & Ayakkabı"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar/giyim-ayakkabi"));

        assertTrue(categoryNameList.contains("Elektronik"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar/elektronik"));


        assertTrue(categoryNameList.contains("Ev & Yaşam"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar/ev-yasam"));

        assertTrue(categoryNameList.contains("Anne & Bebek"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar/anne-bebek"));

        assertTrue(categoryNameList.contains("Kozmetik & Kişisel Bakım"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar/kozmetik-kisisel-bakim"));

        assertTrue(categoryNameList.contains("Mücevher & Saat"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar/mucevher-saat"));

        assertTrue(categoryNameList.contains("Spor & Outdoor"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar/spor-outdoor"));

        assertTrue(categoryNameList.contains("Kitap, Müzik, Film, Oyun"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar/kitap-muzik-film-oyun"));

        assertTrue(categoryNameList.contains("Tatil & Eğlence"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar/tatil-eglence"));

        assertTrue(categoryNameList.contains("Otomotiv & Motosiklet"));
        assertTrue(categoryUrlList.contains("http://www.n11.com/cok-satanlar/otomotiv-motosiklet"));
    }

}
