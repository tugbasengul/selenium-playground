package com.tugba;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by tugba.sengul on 15/03/16.
 */
public class LoginBuyerTest {

    private WebDriver webDriver;

    @Before
    public void setUp() {
        webDriver = new FirefoxDriver();
        webDriver.get("https://www.n11.com/giris-yap");
    }



    @Test
    public void shouldBeLogin() {
        login("tugba.sengul@n11.com", "1q2w3e");
        WebElement buyer = webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div/div[2]/div[2]/div[1]/div[1]/a[2]"));

        assertThat(buyer.getText(), equalTo("Tuğba Şengül"));
    }

    @Test
    public void shouldNotBeLogin() {
        login("tugba.sengul@n11.com", "!1q2w3e");
        WebElement errorText = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[2]/div/div"));
        assertThat(errorText.getText(), equalTo("E-posta adresiniz veya şifreniz hatalı"));


    }

    public void login(String emailValue, String passwordValue) {
        WebElement email = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[1]/input"));
        email.sendKeys(emailValue);

        WebElement password = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[2]/input"));
        password.sendKeys(passwordValue);

        WebElement loginButton = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[5]"));
        loginButton.submit();
    }


    @After
    public void tearDown() throws Exception {
        webDriver.close();
        webDriver.quit();

    }
}
