package com.tugba;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by tugba.sengul on 15/03/16.
 */

public class MemberInformationTest {

    @Test
    public void shouldUpdateUsername() {



        WebDriver webDriver = new FirefoxDriver();

        webDriver.get("https://www.n11.com/giris-yap");


        WebElement email = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[1]/input"));
        email.sendKeys("tugba.sengul@n11.com");

        WebElement password = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[2]/input"));
        password.sendKeys("1q2w3e");

        WebElement loginButton = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[5]"));
        loginButton.submit();


        webDriver.getCurrentUrl();

        WebElement buyer = webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div/div[2]/div[2]/div[1]/div[1]/a[2]"));

        assertThat(buyer.getText(),equalTo("Tuğba Şengül"));

        webDriver.get("https://www.n11.com/hesabim/bilgi-guncelleme");

        WebElement username = webDriver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[1]/form/div[2]/div[1]/input"));
        username.clear();
        username.sendKeys("Tuğçe");

        WebElement savebuton = webDriver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[2]/a[2]"));
        savebuton.click();

        WebElement confirm = webDriver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div"));
        assertThat(confirm.getText(),equalTo("Üyelik bilgileriniz başarıyla güncellendi."));

        WebElement buyerNameCheck = webDriver.findElement(By.xpath("/html/body/div[2]/header/div/div/div[2]/div[2]/div[1]/div[1]/a[2]"));

        assertThat(buyerNameCheck.getText(),equalTo("Tuğçe Şengül"));

        //webDriver.close();
        //webDriver.quit();





    }

    @Test

    public void backUpdateUsarname() {

    WebDriver webDriver = new FirefoxDriver();

    webDriver.get("https://www.n11.com/giris-yap");


    WebElement email = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[1]/input"));
    email.sendKeys("tugba.sengul@n11.com");

    WebElement password = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[2]/input"));
    password.sendKeys("1q2w3e");

    WebElement loginButton = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[5]"));
    loginButton.submit();


    webDriver.getCurrentUrl();

    WebElement buyer = webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div/div[2]/div[2]/div[1]/div[1]/a[2]"));


    webDriver.get("https://www.n11.com/hesabim/bilgi-guncelleme");

    WebElement username = webDriver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[1]/form/div[2]/div[1]/input"));
    username.clear();
    username.sendKeys("Tuğba");

    WebElement savebuton = webDriver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div[2]/a[2]"));
    savebuton.click();


    //WebElement confirm = webDriver.findElement(By.xpath("/html/body/div[2]/div/div[1]/div[2]/div/div[2]/div/div/div"));
    //assertThat(confirm.getText(),equalTo("Üyelik bilgileriniz başarıyla güncellendi."));

    WebElement buyerNameCheck = webDriver.findElement(By.xpath("/html/body/div[2]/header/div/div/div[2]/div[2]/div[1]/div[1]/a[2]"));

    assertThat(buyerNameCheck.getText(),equalTo("Tuğba Şengül"));

    //webDriver.close();
    //webDriver.quit();









}
}










