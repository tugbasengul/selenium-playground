package com.tugba;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;


/**
 * Created by tugba.sengul on 29/06/16.
 */
public class Restart {

    @Test
    public void goST() {


        WebDriver webDriver = new FirefoxDriver();
        webDriver.get("http://st.n11.com/");

        WebElement enterKey = webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div/div[2]/div[1]/div[1]/form/input[1]"));
        enterKey.sendKeys("apple");

        WebElement clickSearchButton = webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div/div[2]/div[1]/a"));
        clickSearchButton.click();

        WebElement resultText = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/section/div[1]/div[1]"));
        assertThat(resultText.getText(),equalTo("Apple\niçin 64,224 sonuç bulundu."));

        webDriver.close();


    }

    @Test
    public void goProductDetail(){

        WebDriver webDriver = new FirefoxDriver();
        webDriver.get("http://st.n11.com/arama?q=apple");

        WebElement productClick = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/section/div[2]/ul/li[1]/div"));
        productClick.click();


        webDriver.close();

    }
}
