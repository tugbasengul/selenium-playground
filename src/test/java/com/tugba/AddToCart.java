package com.tugba;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Created by tugba.sengul on 22/03/16.
 */
public class AddToCart {

    @Test
    public void addToCart() throws InterruptedException {

        WebDriver webDriver = new FirefoxDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        webDriver.get("http://urun.n11.com/budama-makasi/fiskars-115562-2400-4100mm-teleskopik-yuksek-dal-budama-makasi-P45121219");

        WebElement addToCartButton = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div[2]/div[3]/div[3]/a[2]"));
        addToCartButton.click();

        webDriver.get("http://www.n11.com/sepetim");

        WebElement incrementCount = (new WebDriverWait(webDriver, 10))
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div[1]/div/div[3]/table/tbody/tr[3]/td[4]/div/form/div/a[1]")));
        incrementCount.click();


        takeScreenShot(webDriver,"tugba");

        webDriver.quit();
    }

    public static void takeScreenShot(WebDriver driver, String fileName) {
        try {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("/Users/tugba.sengul/Desktop/" + fileName + ".png"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
