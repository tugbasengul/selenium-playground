package com.tugba;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import static org.junit.Assert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by tugba.sengul on 16/03/16.
 */
public class SearchTest {

    @Test
    public void searchTest() {

        WebDriver webDriver = new FirefoxDriver();

        webDriver.get("http://www.n11.com/");

        WebElement searchKeyword = webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div/div[2]/div[1]/div[1]/form/input[1]"));

        searchKeyword.sendKeys("Etek");

        WebElement searchButton = webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div/div[2]/div[1]/a"));

        searchButton.click();

        WebElement searchResultText =webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/section/div[1]/div[1]"));
        assertThat(searchResultText.getText(),equalTo("Etek\n" + "için 9,357 sonuç bulundu."));

        webDriver.close();
        webDriver.quit();




    }
}
