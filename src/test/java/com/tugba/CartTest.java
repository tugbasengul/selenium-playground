package com.tugba;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;


/**
 * Created by tugba.sengul on 16/03/16.
 */
public class CartTest {

    @Test
    public void shouldAddAProductToCart() {

        WebDriver webDriver = new FirefoxDriver();

        webDriver.manage().window().maximize();

        webDriver.get("https://www.n11.com/giris-yap");

        WebElement enterUsername = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[1]/input"));
        enterUsername.sendKeys("tugba.sengul@n11.com");

        WebElement enterPassword = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[2]/input"));
        enterPassword.sendKeys("1q2w3e");

        WebElement girisYap = webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div[1]/div/div/div[1]/form/div[5]"));
        girisYap.click();

        WebElement checkLogin = webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div/div[2]/div[2]/div[1]/div[1]/a[2]"));
        assertThat(checkLogin.getText(),equalTo("Tuğba Şengül"));

        WebElement searchKeyword = webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div/div[2]/div[1]/div[1]/form/input[1]"));
        searchKeyword.sendKeys("etek");

        WebElement clickSearchButton = webDriver.findElement(By.xpath("/html/body/div[1]/header/div/div/div[2]/div[1]/a"));
        clickSearchButton.click();

        WebElement searchResultText = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div/div[2]/section/div[1]/div[1]"));
        //assertThat(searchResultText.getText(),equalTo("Etek\n" + "için 9,384 sonuç bulundu."));


        WebElement firstProduct = webDriver.findElement(By.xpath("//*[@id=\"p-110351181\"]/div[1]"));
        firstProduct.click();

        //WebElement webElementSize = (new WebDriverWait(webDriver, 10))
         //       .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/div/div/div[3]/div[2]/form/div/fieldset[1]/select")));



        //Select color = new Select(webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[2]/form/div/fieldset[2]/select")));
        //color.selectByValue("SİYAH");

        Select size = new Select(webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[2]/div[2]/form/div/fieldset[2]/select")));
        size.selectByValue("36");


        WebElement addToCartButton = webDriver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[2]/div[3]/div[3]/a[2]"));
        addToCartButton.click();


        webDriver.close();
        webDriver.quit();

    }
}
