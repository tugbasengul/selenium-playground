package com.tugba;
import java.util;


/**
 * Created by tugba.sengul on 15/07/16.
 */
public class Test {
    package metrobus;
    import java.util.*;//Scanner'ı kullanabilmek için util kütüphanesini projemize dahil ediyoruz.
    //http://www.programlamadersleri.com
    public class Metrobus {//Metrobus adında classımızı oluşturduk

        static double bakiye=0;//Bakiyemizi tanımlıyoruz.
        static void ParaYatir(){//para yatır methodumuzu tanımlıyoruz
            System.out.println("Yatırmak istediğiniz miktarı giriniz");
            //Kullanıcıdan para yatırmasını istiyoruz.
            Scanner oku=new Scanner(System.in);
            //Kullanıcının yatırdığı parayı okuyoruz
            bakiye = bakiye + oku.nextInt();
            //mevcut bakiyeye kullanıcının yatırdığı miktarı ekliyoruz
        }

        static void BakiyeGoster(){
//Eğer kullanıcı para miktarını sorgulamak isterse bu metoda geliyor.
            System.out.println("Bakiyeniz:" + bakiye);
        } //http://www.programlamadersleri.com

        static void Metrobus(){//Metrobus methodumuzu oluşturuyoruz
//2 adet karttan birini seçmesini istiyoruz
            System.out.println("1-Istanbul Kart");
            System.out.println("2-Indirimli Kart");
            Scanner oku=new Scanner(System.in);
            //secim işlemini okuyoruz
            int secim=1;
            secim= oku.nextInt();
            if(secim==1)
            {//eğer seçim 1 ise
                if(bakiye>=3.10){//Eğer kartta ki para 3.10 değerinden yüksek ise
                    bakiye=bakiye - 3.10;// bakiyeden 3.10 düşüyoruz
                    System.out.println("34BZ - Beylikdüzü - Zincirlikuyu. \\n Lütfen kapılara yaslanmayınız...");
                }
                else if (bakiye < 3.10)// eğer bakiyesi 3.10dan yüksek değil ise
                {
                    System.out.println("Bakiye Yetersiz");
                }
            }
            else if(secim==2)
            { //http://www.programlamadersleri.com
                if(bakiye>=1.10){
                    bakiye=bakiye - 1.10;
                    //Bakiyemizi düştükten sonra mesaj veriyoruz.
                    System.out.println("34BZ - Beylikdüzü - Zincirlikuyu. \\n Lütfen yaşlılara yer veriniz...");
                }
                else if (bakiye < 1.10)
                {
                    System.out.println("Bakiye Yetersiz");
                }
            }
            //http://www.programlamadersleri.com
        }


        public static void main(String[] args) {
            int secim=0;//Değişkene başlangıç değeri ekliyoruz.

            do {//Do kullanarak işlemleri en az 1 kez gösteriyoruz.
                System.out.println("1-Karta Para Yatır");
                System.out.println("2-Bakiye Goster");
                System.out.println("3-Metrobüse Bin");
                System.out.println("4-Uygulamayı Kapat");
                Scanner oku=new Scanner(System.in);
                secim=oku.nextInt();
                //kullanıcının yapmak istediği işlemi seçime aktarıyoruz
                switch  (secim)
                {//Seçimin değerine göre işlem yapıyoruz
                    case 1: //Eğer seçim 1 ise
                        ParaYatir();
                        break;
                    case 2:
                        BakiyeGoster();
                        break;
                    case 3:
                        Metrobus();
                        break;
                    case 4:
                        System.exit(0);//Uygulamayı kapatıyoruz.
                        break;

                }
            } while (secim!=4);//Seçim 4 olana dek kullanıcıya işlem seçeneklerini gösteriyoruz.
            //secim 4 olduğunda ise uygulama kapatılıyor.

        }
    }
}
